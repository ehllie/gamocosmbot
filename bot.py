from discord.ext import commands
from cogs.moderation import GCPermissions
import pickle
from cogs.api_requests import GamocosmApi


class GCBot(commands.Bot):

    @staticmethod
    def load_channels():
        try:
            with open("channels", "rb") as ifile:
                channels = pickle.load(ifile)
        except (FileNotFoundError, EOFError):
            with open("channels", "wb") as ofile:
                channels = set()
                pickle.dump(channels, ofile)
        return channels

    @staticmethod
    def save_channels(channels):
        with open("channels", "wb") as ofile:
            pickle.dump(channels, ofile)

    def __init__(self, owner_id, api_key, server_id, *args, **kwargs):
        super(GCBot, self).__init__(*args, **kwargs)
        allowed_channels = self.load_channels()
        self.add_cog(GCPermissions(self, owner_id, allowed_channels))
        self.add_cog(GamocosmApi(self, api_key, server_id))

    async def logout(self):
        self.save_channels(self.get_cog("GCPermissions").allowed_channels)
        await super(GCBot, self).logout()

