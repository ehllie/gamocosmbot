import configparser
import sys
from bot import GCBot

parser = configparser.ConfigParser()
try:
    parser.read("bot.conf")
    token = parser.get("Discord Bot", "token")
    prefix = parser.get("Discord Bot", "prefix")
    owner_id = parser.get("Discord Bot", "owner_id")
    api_key = parser.get("Gamocosm Api", "api_key")
    server_id = parser.get("Gamocosm Api", "server_id")
except configparser.NoSectionError:
    with open("bot.conf", "w") as config_file:
        token = "Your discord bot token"
        prefix = "yc!"
        owner_id = "Your own discord id"
        api_key = "Gamocosm api key"
        server_id = "Gamocosm server id"
        parser.add_section("Discord Bot")
        parser.add_section("Gamocosm Api")
        parser.set("Discord Bot", "token", token)
        parser.set("Discord Bot", "prefix", prefix)
        parser.set("Discord Bot", "owner_id", owner_id)
        parser.set("Gamocosm Api", "api_key", api_key)
        parser.set("Gamocosm Api", "server_id", server_id)
        print("Please fill out the config file")
        sys.exit(0)


gcbot = GCBot(owner_id=int(owner_id), api_key=api_key, server_id=server_id, command_prefix=prefix)

gcbot.run(token)

