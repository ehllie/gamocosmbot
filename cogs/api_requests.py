import json
import requests
from cogs.moderation import permit_level, GCCogs
from discord.ext import commands


class GamocosmApi(GCCogs):

    def __init__(self, bot, api_key, server_id):
        self.url = f"https://gamocosm.com/servers/{server_id}/api/{api_key}/"
        super(GamocosmApi, self).__init__(bot)

    def __get_data(self, endpoint):
        data = requests.get(self.url+endpoint)
        return data

    def __post_data(self, endpoint):
        data = requests.post(self.url+endpoint, "")
        return data

    @staticmethod
    def __load(data):
        return json.loads(data.content.decode(encoding="utf8"))

    @commands.command()
    @permit_level(1)
    async def status(self, ctx: commands.Context):
        content = self.__load(self.__get_data("status"))
        await ctx.send(f"`{content}`")

    @commands.command()
    @permit_level(1)
    async def start(self, ctx: commands.Context):
        content = self.__load(self.__post_data("start"))
        await ctx.send(f"`{content}`")

    @commands.command()
    @permit_level(3)
    async def stop(self, ctx: commands.Context):
        content = self.__load(self.__post_data("stop"))
        await ctx.send(f"`{content}`")

    @commands.command()
    @permit_level(3)
    async def pause(self, ctx: commands.Context):
        content = self.__load(self.__post_data("pause"))
        await ctx.send(f"`{content}`")

    @commands.command()
    @permit_level(3)
    async def resume(self, ctx: commands.Context):
        content = self.__load(self.__post_data("resume"))
        await ctx.send(f"`{content}`")

    @commands.command()
    @permit_level(1)
    async def backup(self, ctx: commands.Context):
        content = self.__load(self.__post_data("backup"))
        await ctx.send(f"`{content}`")
