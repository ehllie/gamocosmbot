from discord.ext import commands
import functools
import discord


class GCCogs(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    def check_permission(self, ctx):
        permissions: GCPermissions = self.bot.get_cog("GCPermissions")
        return permissions.check_permission(ctx)


def permit_level(level):

    def permit_level_decorator(func):
        @functools.wraps(func)
        async def wrapper(self, ctx, **kwargs):
            perms = await self.check_permission(ctx)
            if perms >= level:
                await func(self, ctx, **kwargs)
            else:
                raise commands.MissingPermissions(["manage_channels"])

        return wrapper
    return permit_level_decorator


class GCPermissions(GCCogs):
    def __init__(self, bot, owner_id, allowed_channels):
        self.owner_id = owner_id
        self.allowed_channels: set = allowed_channels
        super(GCPermissions, self).__init__(bot)

    async def check_permission(self, ctx: commands.Context):
        author: discord.Member = ctx.author
        if author.id == self.owner_id:
            return 3
        elif author.permissions_in(ctx.channel).manage_channels:
            return 2
        elif ctx.channel.id not in self.allowed_channels:
            return 0
        else:
            return 1

    @commands.Cog.listener()
    async def on_ready(self):
        print("The bot is ready")

    @commands.command()
    @permit_level(2)
    async def add_channel(self, ctx):
        if ctx.channel.id in self.allowed_channels:
            await ctx.send("Users can already talk in this channel")
        else:
            self.allowed_channels.add(ctx.channel.id)
            await ctx.send("Users can now talk in this channel")

    @commands.command()
    @permit_level(2)
    async def remove_channel(self, ctx):
        if ctx.channel.id in self.allowed_channels:
            self.allowed_channels.remove(ctx.channel.id)
            await ctx.send("Users can no longer talk in this channel")
        else:
            await ctx.send("This channel is not one of the allowed channels")

    @commands.command()
    @permit_level(3)
    async def shutdown(self, ctx):
        await ctx.send("Shutting down")
        await self.bot.logout()

